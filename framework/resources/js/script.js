var app = angular.module("myApp", [])
app.controller('chiroCtrl', function ($scope, $http) {
    $http.get('http://193.191.187.14:10258/api')
        .success(function (response) {
            $scope.myData = response;
        })
        .error(function (response, status) {
            alert('Error retrieving JSON -- ' + status);
        })
});

var createApp = angular.module("createApp", []);
createApp.controller("createCtrl", ['$http', '$scope', function ($http, $scope) {
    $scope.save = function (chiro) {
        $http({
            url: 'http://193.191.187.14:10258/api',
            method: 'POST',
            data: JSON.stringify(chiro),
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(
            function success() {
            window.location.href = "overview.html"
        }, function error() {
            $(".errorForm").text("Please fill in the form correct!")
        })
    }
}])
