package facade;

import db.ChiroDB.IChiroDB;
import db.DbException;
import db.Factory;
import db.MemberDB.IMemberDB;
import model.Chiro;
import model.Member;

import java.util.List;

/**
 * Created by Jan on 10/02/2016.
 */
public class Facade{

    private IChiroDB chiroDB;
    private IMemberDB memberDB;

    public Facade( String type ) throws DbException{
        this.chiroDB = Factory.createChiroDB( type );
        this.memberDB = Factory.createMemberDB( type );
        init();
    }

    private void init(){
        //DUMMY DATA
        // TODO REMOVE ME!

        Chiro gerolf = new Chiro( "Sint-Gerolf Genk Centrum", "gerolf@abc.be", "Genk" );

        Member jan = new Member( "Jan", "Pecquet", 1995, "jan@me.be" );
        jan.setChiroName( "Sint-Gerolf Genk Centrum" );

        Member karel = new Member( "Karel", "Karels", 2003, "karel@me.be" );
        karel.setChiroName( "Sint-Gerolf Genk Centrum" );

        Member tom = new Member( "Tom", "Trompet", 2005, "tom@me.be" );
        tom.setChiroName( "Sint-Gerolf Genk Centrum" );

        Member humpie = new Member( "Humpie", "Stumpie", 1999, "humpie@me.be" );
        humpie.setChiroName( "Sint-Gerolf Genk Centrum" );

        addChiro( gerolf );

        addMember( jan );
        addMember( karel );
        addMember( tom );
        addMember( humpie );


    }

    // CHIRO

    public Chiro getChiro( String name ) throws DbException{
        return this.chiroDB.getChiro( name );
    }

    public List<Chiro> getAllChiros() throws DbException{
        return this.chiroDB.getAll();
    }

    public void addChiro( Chiro chiro ) throws DbException{
        this.chiroDB.addChiro( chiro );
    }

    public void deleteChiro( String name ) throws DbException{
        this.chiroDB.deleteChiro( name );
    }

    public void deleteMemberFromChiro( Member member ){
        chiroDB.deleteMemberFromChiro( member );
    }


    // MEMBER

    public Member getMember( String firstName, String lastName ) throws DbException{
        return memberDB.getMember( firstName, lastName );
    }

    public List<Member> getAllMembers() throws DbException{
        return memberDB.getAll();
    }

    public void addMember( Member member ) throws DbException{
        memberDB.addMember( member );
        chiroDB.addMemberToChiro( member );
    }

    public void deleteMember( String firstName, String lastName ) throws DbException{
        deleteMemberFromChiro( getMember( firstName, lastName ) );
        memberDB.deleteMember( firstName, lastName );
    }

    public void updateMember( Member member ){
        memberDB.updateMember( member );
    }


}
