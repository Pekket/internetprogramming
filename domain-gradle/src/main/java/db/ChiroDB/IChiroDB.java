/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.ChiroDB;

import db.DbException;
import model.Chiro;
import model.Member;

import java.util.List;

public interface IChiroDB{

    Chiro getChiro( String name ) throws DbException;

    List<Chiro> getAll() throws DbException;

    void addChiro( Chiro chiro ) throws DbException;

    void deleteChiro( String name ) throws DbException;

    void addMemberToChiro( Member member ) throws DbException;

    void deleteMemberFromChiro( Member member ) throws DbException;
}
