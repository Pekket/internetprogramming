
package db.ChiroDB;

import db.DbException;
import model.Chiro;
import model.Member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChiroMap implements IChiroDB{

    private Map<String, Chiro> chiros;

    public ChiroMap(){
        chiros = new HashMap<String, Chiro>();
    }

    @Override
    public Chiro getChiro( String name ) throws DbException{
        if( name == null || name.equals( "" ) || !chiros.containsKey( name ) )
            throw new DbException( "Invalid id : Cannot get Chiro" );
        return this.chiros.get( name );
    }

    @Override
    public List<Chiro> getAll(){
        return new ArrayList(chiros.values());
    }

    @Override
    public void addChiro( Chiro chiro ) throws DbException{
        if( chiro == null )
            throw new DbException( "Cannot add empty Chiro" );
        chiros.put( chiro.getName(), chiro );
    }

    @Override
    public void deleteChiro( String name ) throws DbException{
        if( name == null || name.equals( "" ) || !chiros.containsKey( name ) )
            throw new DbException( "Invalid name : Cannot delete Chiro" );
        this.chiros.remove( name );
    }

    @Override
    public void addMemberToChiro( Member member ){
        if( member == null || !chiros.containsKey( member.getChiroName() ) )
            throw new DbException( "Cannot add empty Member " );
        getChiro( member.getChiroName() ).addMember( member );
    }

    @Override
    public void deleteMemberFromChiro( Member member ) throws DbException{
        if( member == null || !chiros.containsKey( member.getChiroName() ) )
            throw new DbException( "Cannot delete member from chiro" );

        chiros.get( member.getChiroName() ).deleteMember( member );
    }


}
