package db.MemberDB;

import db.DbException;
import model.Member;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan on 15/02/2016.
 */
public class MemberMap implements IMemberDB{

    private List<Member> members;

    public MemberMap(){
        members = new ArrayList<Member>();
    }

    @Override
    public Member getMember( String firstName, String lastName ) throws DbException{
        if(firstName == null || firstName.equals( "" ) || lastName == null || lastName.equals( "" ))
            throw new DbException( "Cannot find member with empty firstname or lastname" );

        for(Member m : members) {
            if( m.getFirstName().toLowerCase().equals( firstName.toLowerCase() ) && m.getLastName().toLowerCase().equals( lastName.toLowerCase() ))
                return m;
        }
        return null;
    }

    @Override
    public List<Member> getAll() throws DbException{
        return members;
    }

    @Override
    public void addMember( Member member ) throws DbException{
        if(member == null)
            throw new DbException( "Cannot add empty member" );
        members.add( member );
    }

    @Override
    public void deleteMember( String firstName, String lastName ) throws DbException{
        if(firstName == null ||lastName == null)
            throw new DbException( "Cannot find member with empty firstname or lastname" );

        for(Member m : members) {
            if(m.getFirstName().toLowerCase().equals( firstName.toLowerCase() ) && m.getLastName().toLowerCase().equals( lastName.toLowerCase() )) {
                members.remove( m );
                return;
            }
        }
    }

    @Override
    public void updateMember( Member member ){
        if(member == null)
            throw new DbException( "Cannot update empty member" );

        deleteMember( member.getFirstName(), member.getLastName() );
        addMember( member );
    }
}
