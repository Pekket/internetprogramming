package db.MemberDB;

import db.DbException;
import model.Member;

import java.util.List;

/**
 * Created by Jan on 15/02/2016.
 */
public interface IMemberDB{

    Member getMember (String firstName, String lastName) throws DbException;

    List<Member> getAll() throws DbException;

    void addMember( Member member) throws DbException;

    void deleteMember(String firstName, String lastName) throws DbException;

    void updateMember(Member member);

}
