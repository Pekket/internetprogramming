/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;


import db.ChiroDB.ChiroMap;
import db.ChiroDB.ChiroRDB;
import db.ChiroDB.IChiroDB;
import db.MemberDB.IMemberDB;
import db.MemberDB.MemberMap;
import db.MemberDB.MemberRDB;

public class Factory{

    public static IChiroDB createChiroDB( String type ){
        IChiroDB db = null;
        if( type.equals( "MAP" ) ){
            db = new ChiroMap();
        }else if( type.equals( "DB" ) ){
            db = new ChiroRDB();
        }
        return db;
    }


    public static IMemberDB createMemberDB( String type ){
        IMemberDB db = null;
        if( type.equals( "MAP" ) ){
            db = new MemberMap();
        }else if( type.equals( "DB" ) ){
            db = new MemberRDB();
        }
        return db;
    }

}
