
package model;

import java.util.Calendar;

public class Member{

    private String chiroName;

    private String firstName;
    private String lastName;

    private int birthYear;
    private String email;

    public Member( String firstName, String lastName, int birthyear, String email ){
        setFirstName( firstName );
        setLastName( lastName );
        setBirthYear( birthyear );
        setEmail( email );
    }

    public String calculateGroup(){
        int CurrentYear = Calendar.getInstance().get( Calendar.YEAR );
        int group = CurrentYear - birthYear;

        switch( group ){
            case 6:
            case 7:
            case 8:
                return "Speelclubs";
            case 9:
            case 10:
            case 11:
                return "Rakkers";
            case 12:
            case 13:
                return "Toppers";
            case 14:
            case 15:
                return "Kerels";
            case 16:
            case 17:
                return "Aspirant";
            default:
                return "Leider";
        }
    }

    public String getFirstName(){
        return firstName;
    }

    private void setFirstName( String firstname ){
        this.firstName = firstname;
    }

    public String getLastName(){
        return lastName;
    }

    private void setLastName( String lastName ){
        this.lastName = lastName;
    }

    public int getBirthYear(){
        return birthYear;
    }

    public void setBirthYear( int birthyear ){
        this.birthYear = birthyear;
    }

    public String getEmail(){
        return email;
    }

    private void setEmail( String email ){
        this.email = email;
    }

    public String getChiroName(){
        return chiroName;
    }

    public void setChiroName( String chiroName ){
        this.chiroName = chiroName;
    }
}
