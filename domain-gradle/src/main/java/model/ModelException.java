/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class ModelException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ModelException(){
        super();
    }

    public ModelException( String message ){
        super( message );
    }

    public ModelException( String message, Throwable exception ){
        super( message, exception );
    }

}
