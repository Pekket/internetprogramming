package model;

import java.util.ArrayList;
import java.util.List;

public class Chiro{

    private String name;
    private String email;
    private String city;

    private List<Member> members;

    public Chiro( String name, String email, String city ){
        members = new ArrayList<Member>();
        setName( name );
        setEmail( email );
        setCity( city );
    }

    public void addMember( Member member ){
        if( member == null )
            throw new ModelException( "Cannot add empty member to Chiro" );
        members.add( member );
    }

    public void deleteMember( Member member ){
        if( member == null )
            throw new ModelException( "Cannot delete empty member to Chiro" );
        members.remove( member );
    }

    public List<Member> getMembers(){
        return members;
    }

    public String getName(){
        return name;
    }

    private void setName( String name ) throws ModelException{
        if( name == null || name.equals( "" ) )
            throw new ModelException( "invalid naam" );

        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    private void setEmail( String email ) throws ModelException{
        if( email == null || email.equals( "" ) )
            throw new ModelException( "invalid email" );

        this.email = email;
    }

    public String getCity(){
        return city;
    }

    private void setCity( String city ) throws ModelException{
        if( city == null || city.equals( "" ) )
            throw new ModelException( "invalid stad" );

        this.city = city;
    }


}
