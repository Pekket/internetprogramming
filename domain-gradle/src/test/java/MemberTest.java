import model.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jan on 15/02/2016.
 */
public class MemberTest{
    Member m;

    @Before
    public void before(){
        m = new Member( "Jan", "Pecquet", 1995, "me@pekket.be" );
    }

    @Test
    public void test_create_member_with_parameters(){
        Assert.assertEquals( "Jan", m.getFirstName() );
        Assert.assertEquals( "Pecquet", m.getLastName() );
        Assert.assertEquals( 1995, m.getBirthYear() );
        Assert.assertEquals( "me@pekket.be", m.getEmail() );
    }

    @Test
    public void test_calculate_group() {
        Assert.assertEquals( "Leider", m.calculateGroup());
        Member m1 = new Member( "Piet", "Pieters", 2004, "piet@piet.be" );
        Assert.assertEquals( "Toppers", m1.calculateGroup());
    }


}
