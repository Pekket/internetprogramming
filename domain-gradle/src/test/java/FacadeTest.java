import facade.Facade;
import db.DbException;
import model.Chiro;
import model.Member;
import org.junit.*;

/**
 * Created by Jan on 10/02/2016.
 */
public class FacadeTest{

    Facade service;
    Chiro chiro;
    Chiro chiro2;

    @Before
    public void before() {
        service = new Facade( "MAP" );

        //already added one chiro in facade
        chiro = new Chiro("BXB", "BXB@chiro.be", "Genk");
        chiro2 = new Chiro("KrisKras", "kk@chiro.be", "Genk");

    }

    //TESTS FOR CHIRODB!

    @Test
    public void test_get_chiro_from_db() {
        Assert.assertEquals( "Sint-Gerolf Genk Centrum", service.getChiro( "Sint-Gerolf Genk Centrum" ).getName() );
    }

    @Test(expected = DbException.class)
    public void test_get_chiro_from_db_empty_name(){
        service.getChiro( "" );
    }

    @Test(expected = DbException.class)
    public void test_get_chiro_from_db_wrong_name(){
        service.getChiro( "Leuven" );
    }

    @Test
    public void test_get_all_chiros_from_db() {
        service.addChiro( chiro );
        Assert.assertEquals( 2, service.getAllChiros().size() );
    }

    @Test
    public void test_add_chiro_to_db() {
        service.addChiro( chiro );
        service.addChiro( chiro2 );
        Assert.assertEquals( 3, service.getAllChiros().size() );
        Assert.assertEquals( "KrisKras",  service.getChiro( "KrisKras" ).getName() );
    }

    @Test(expected = DbException.class)
    public void test_add_empty_chiro_to_db(){
        service.addChiro( null );
    }

    @Test
    public void test_delete_chiro_from_db() {
        service.addChiro( chiro );
        Assert.assertEquals( 2, service.getAllChiros().size() );
        service.deleteChiro( "BXB" );
        Assert.assertEquals( 1, service.getAllChiros().size() );
    }

    @Test(expected = DbException.class)
    public void test_delete_empty_chiro_to_db(){
       service.deleteChiro( "" );
    }

    @Test(expected = DbException.class)
    public void test_delete_chiro_to_db_wrong_name(){
       service.deleteChiro( "ApenLand" );
    }


    // TEST FOR MEMBERS


    @Test
    public void test_get_member_from_db() {
        Assert.assertEquals( "jan", service.getMember( "Jan", "Pecquet" ).getFirstName().toLowerCase() );
    }

    @Test
    public void test_get_member_from_db_uppercase() {
        Assert.assertEquals( "jan", service.getMember( "JAN", "PECQUET" ).getFirstName().toLowerCase() );
    }

    @Test(expected = Exception.class)
    public void test_get_member_from_db_empty_firstname(){
        service.getMember( "", "Pecquet" );
    }

    @Test(expected = Exception.class)
    public void test_get_member_from_db_empty_lastname(){
        service.getMember( "jan", "" );
    }

    @Test
    public void test_get_all_members_from_db() {
        Assert.assertEquals( 4, service.getAllMembers().size() );
    }

    @Test
    public void test_add_member_to_db() {
        Member m = new Member( "cool", "boy", 2001, "cool@boy.be" );
        m.setChiroName( "Sint-Gerolf Genk Centrum" );
        service.addMember(m);
        Assert.assertEquals( 5, service.getAllMembers().size() );
    }

    @Test(expected = Exception.class)
    public void test_add_empty_member_to_db(){
        service.addMember( null );
    }

    @Test
    public void test_delete_member_from_db() {
        service.deleteMember( "Jan", "Pecquet" );
        Assert.assertEquals( 3, service.getAllMembers().size() );
    }

    @Test(expected = Exception.class)
    public void test_delete_empty_member_from_db() {
        service.deleteMember( "", "" );
    }


    @Test
    public void test_update_member() {
        Member m = service.getMember( "Jan", "Pecquet" );
        Assert.assertEquals( 1995, service.getMember( "Jan", "Pecquet" ).getBirthYear() );
        m.setBirthYear( 2005 );
        service.updateMember( m );
        Assert.assertEquals( 2005, service.getMember( "Jan", "Pecquet" ).getBirthYear() );
    }

    @Test(expected = Exception.class)
    public void test_update_empty_member_from_db() {
        service.updateMember( null );
    }
}