<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><spring:message code="title.index" /></title>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/sample.css"/>" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Chiro</a>
            </div>
        
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<c:url value="/chiro" />" ><spring:message code="link.overview" /></a>
                    </li>
                    <li>
                        <a href="<c:url value="/chiro/new"/>"><spring:message code="link.create" /></a>
                    </li>                
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1><spring:message code="home.title" /></h1> 
                <p class="lead"><spring:message code="home.description" /></p>
                
            </div>
            <div class="col-lg-12 text-center lang">
                <h3><spring:message code="home.languages" /></h3>
                <a href="<c:url value="?lang=nl"/>"><p><spring:message code="lang.nl" /></p></a>
                <a href="<c:url value="?lang=en"/>"><p><spring:message code="lang.en" /></p></a>
            </div>    
                
        </div>

    </div>
                    
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>






