<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><spring:message code="member.create"/></title>

    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/sample.css"/>" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Chiro</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<c:url value="/chiro" />" ><spring:message code="link.overview" /></a>
                </li>
            </ul>
        </div>

    </div>
</nav>
<div class="container">

    <div class="row">
        <div class="col-lg-12 text-center">
            <h1><spring:message code="member.create"/></h1>
            <div>
                <form:form commandName="member" method="post" action="/spring/member/create.htm">
                    <table id="create-table">
                        <form:hidden path="ID"/>
                        <form:hidden path="ChiroID"/>

                        <tr>
                            <td><spring:message code="create.member.firstname"/></td>
                            <td>
                                <form:input path="firstName" maxlength="25"/>
                                <span class="form-error"><form:errors path="firstName"/></span>
                            </td>
                        </tr>

                        <tr>
                            <td><spring:message code="create.member.lastname"/></td>
                            <td>
                                <form:input path="lastName" maxlength="25"/>
                                <span class="form-error"><form:errors path="lastName"/></span>
                            </td>
                        </tr>

                        <tr>
                            <td><spring:message code="create.member.birthyear"/></td>
                            <td>
                                <form:input path="birthYear" maxlength="25" type="number" min="1950" max="2016"/>
                                <span class="form-error"><form:errors path="birthYear"/></span>
                            </td>
                        </tr>

                        <tr>
                            <td><spring:message code="create.member.email"/></td>
                            <td>
                                <form:input path="Email" type="email" maxlength="35"/>
                                <span class="form-error"><form:errors path="email"/></span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <input type="submit" value="<spring:message code="create.submit" />">
                            </td>
                        </tr>

                    </table>
                </form:form>
            </div>
        </div>
    </div>

</div>

<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>

</body>

</html>
