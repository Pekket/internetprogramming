<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><spring:message code="title.chirooverview"/></title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/sample.css"/>" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Chiro</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<c:url value="/chiro/new"/>"><spring:message code="link.create"/></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="row">
        <div class="col-lg-12 text-center">
            <h1><spring:message code="title.chirooverview"/></h1>
            <div class="overviewcenter">
                <table id="overview">
                    <tr>
                        <th><spring:message code="chiro.name"/></th>
                        <th><spring:message code="chiro.email"/></th>
                        <th><spring:message code="chiro.city"/></th>
                        <th><spring:message code="chiro.weather"/></th>
                        <th><spring:message code="chiro.members"/></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <c:forEach var="chiro" items="${chiros}">
                        <tr>
                            <td><c:out value="${chiro.name}"/></td>
                            <td><c:out value="${chiro.email}"/></td>
                            <td><c:out value="${chiro.city}"/></td>
                            <td><c:out value="${chiro.weather}"/></td>
                            <td><a href="<c:url value="/member/${chiro.id}"/>"><img
                                    src="<c:url value="/resources/img/list.png"/>" id="picto"></a></td>
                            <td><a href="<c:url value="/chiro/update/${chiro.id}"/>"><img
                                    src="<c:url value="/resources/img/edit.png"/>" id="picto"></a></td>
                            <td><a href="<c:url value="/chiro/delete/${chiro.id}"/>" title="delete"><img
                                    src="<c:url value="/resources/img/delete.png"/>" id="picto"></a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>

        </div>
    </div>

</div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>

</html>























