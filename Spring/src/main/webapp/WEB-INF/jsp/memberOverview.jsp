<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><spring:message code="title.memberoverview"/></title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/sample.css"/>" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Chiro</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<c:url value="/chiro"/>"><spring:message code="link.overview"/></a>
                </li>
                <li>
                    <a href="<c:url value="/member/new"/>"><spring:message code="member.create"/></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1><spring:message code="member.title"/></h1>
            <div class="overviewcenter">
                <table>
                    <tr>
                        <th><spring:message code="member.firstname"/></th>
                        <th><spring:message code="member.lastname"/></th>
                        <th><spring:message code="member.birthyear"/></th>
                        <th><spring:message code="member.email"/></th>
                        <th><spring:message code="member.group"/></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <c:forEach var="member" items="${members}">
                        <tr>
                            <td><c:out value="${member.firstName}"/></td>
                            <td><c:out value="${member.lastName}"/></td>
                            <td><c:out value="${member.birthYear}"/></td>
                            <td><c:out value="${member.email}"/></td>
                            <td><c:out value="${member.calculateGroup()}"/></td>
                            <td><a href="<c:url value="/member/update/${member.ID}"/>" title="edit"><img
                                    src="<c:url value="/resources/img/edit.png"/>" id="picto"></a></td>
                            <td><a href="<c:url value="/member/delete/${member.ID}"/>" title="delete"><img
                                    src="<c:url value="/resources/img/delete.png"/>" id="picto"></a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>



















