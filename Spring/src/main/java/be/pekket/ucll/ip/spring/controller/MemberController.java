package be.pekket.ucll.ip.spring.controller;

import be.pekket.ucll.ip.domain.facade.Facade;
import be.pekket.ucll.ip.domain.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@Controller
@RequestMapping( value = "/member" )
public class MemberController{

    @Autowired
    private Facade service;
    private long id;

    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ModelAndView getMembers( @PathVariable long id ){
        this.id = id;
        System.out.println("HIER" + "  " + id);
        return new ModelAndView( "memberOverview", "members", service.getChiro( id ).getMembers() );
    }

    @RequestMapping( value = "/new", method = RequestMethod.GET )

    public ModelAndView getNewForm(){
        Member m = new Member();
        m.setChiroID( id );
        return new ModelAndView( "memberCreateForm", "member", m );
    }


    @RequestMapping( value = "create", method = RequestMethod.POST )
    public String save( @Valid @ModelAttribute( "member" ) Member member, BindingResult result ){
        if( result.hasErrors() ) return "memberCreateForm";
        service.addMember( member );
        return "redirect:/chiro.htm";
    }

    @RequestMapping(value ="/saveupdate", method=RequestMethod.POST)
    public String saveUpdate( @Valid @ModelAttribute("member") Member member, BindingResult result) {
        if(result.hasErrors()) return "memberUpdateForm";
        service.updateMember( member );
        return "redirect:/chiro.htm";
    }

    @RequestMapping(value="/update/{id}", method=RequestMethod.GET)
    public ModelAndView update(@PathVariable long id) {
        return new ModelAndView("memberUpdateForm", "member", service.getMember( id ));
    }

    @RequestMapping( value = "/delete/{id}", method = RequestMethod.GET )
    public String delete( @PathVariable long id ){
        service.deleteMember( id );
        return "redirect:/chiro.htm";
    }


}
