package be.pekket.ucll.ip.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping( value = "/" )
public class RootController{

    @RequestMapping( method = RequestMethod.GET )
    public ModelAndView getIndex(){
        return new ModelAndView( "index" );
    }

}
