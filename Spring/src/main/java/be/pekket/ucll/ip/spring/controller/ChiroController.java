package be.pekket.ucll.ip.spring.controller;

import be.pekket.ucll.ip.domain.facade.Facade;
import be.pekket.ucll.ip.domain.model.Chiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/chiro" )
public class ChiroController {
    
    @Autowired
    private Facade service;
    
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getChiros(){
        return new ModelAndView("chiroOverview", "chiros", service.getAllChiros());
    }
    
    @RequestMapping(value ="/new", method=RequestMethod.GET)
    public ModelAndView getNewForm() {
        return new ModelAndView("createForm", "chiro", new Chiro());
    }
    
    @RequestMapping(value ="create", method=RequestMethod.POST)
    public String save(@Valid @ModelAttribute("chiro") Chiro chiro, BindingResult result) {
        if(result.hasErrors()) return "createForm";
        service.addChiro(chiro);
        return "redirect:/chiro.htm";
    }
    
    @RequestMapping(value ="saveupdate", method=RequestMethod.POST)
    public String saveUpdate( @Valid @ModelAttribute("chiro") Chiro chiro, BindingResult result) {
        if(result.hasErrors()) return "updateForm";
        service.updateChiro(chiro);
        return "redirect:/chiro.htm";
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public String getMembers(@PathVariable String id) {
        return "redirect:/members/{"+id+"htm}";
    }   
                
    @RequestMapping(value="/update/{id}", method=RequestMethod.GET)
    public ModelAndView update(@PathVariable long id) {
        return new ModelAndView("updateForm", "chiro", service.getChiro(id));
    }    
        
    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public String delete(@PathVariable long id) {
        service.deleteChiro(id);
        return "redirect:/chiro.htm";
    }
    
}
