import be.pekket.ucll.ip.domain.facade.Facade;
import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.facade.ServiceException;
import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;
import org.junit.*;

/**
 * Created by Jan on 10/02/2016.
 */
public class FacadeTest{

    Facade service;
    Chiro chiro;
    Chiro chiro2;

    @Before
    public void before() {
        service = new Facade( "MAP" );

        //already added one chiro in be.pekket.ucll.ip.domain.facade
        chiro = new Chiro("BXB", "BXB@chiro.be", "Genk");
        chiro2 = new Chiro("KrisKras", "kk@chiro.be", "Genk");

    }

    //TESTS FOR CHIRODB!

    @Test
    public void test_get_chiro_from_db() {
        Assert.assertEquals( "Sint-Gerolf Genk Centrum", service.getChiro( 1 ).getName() );
    }

    @Test(expected = ServiceException.class)
    public void test_get_chiro_from_db_empty_name(){
        service.getChiro( 0 );
    }

    @Test(expected = ServiceException.class)
    public void test_get_chiro_from_db_wrong_name(){
        service.getChiro( 8 );
    }

    @Test
    public void test_get_all_chiros_from_db() {
        service.addChiro( chiro );
        Assert.assertEquals( 3, service.getAllChiros().size() );
    }

    @Test
    public void test_add_chiro_to_db() {
        service.addChiro( chiro );
        service.addChiro( chiro2 );
        Assert.assertEquals( 4, service.getAllChiros().size() );
        Assert.assertEquals( "KrisKras",  service.getChiro( 4 ).getName() );
    }

    @Test(expected = Exception.class)
    public void test_add_empty_chiro_to_db(){
        service.addChiro( null );
    }

    @Test
    public void test_delete_chiro_from_db() {
        service.addChiro( chiro );
        Assert.assertEquals( 3, service.getAllChiros().size() );
        service.deleteChiro( 3 );
        Assert.assertEquals( 2, service.getAllChiros().size() );
    }

    @Test(expected = ServiceException.class)
    public void test_delete_empty_chiro_to_db(){
       service.deleteChiro( 0 );
    }

    @Test(expected = ServiceException.class)
    public void test_delete_chiro_to_db_wrong_name(){
       service.deleteChiro( 9 );
    }

    @Test
    public void test_update_chiro(){
        Chiro c = service.getChiro( 1 );
        c.setCity( "Brussel" );
        service.updateChiro( c );
        Assert.assertEquals( "Brussel", service.getChiro( 1 ).getCity() );
    }


    // TEST FOR MEMBERS


    @Test
    public void test_get_member_from_db() {
        Assert.assertEquals( "jan", service.getMember( 1 ).getFirstName().toLowerCase() );
    }

    @Test
    public void test_get_member_from_db_uppercase() {
        Assert.assertEquals( "jan", service.getMember( 1 ).getFirstName().toLowerCase() );
    }

    @Test(expected = Exception.class)
    public void test_get_member_from_db_empty_id(){
        service.getMember( 0 );
    }


    @Test
    public void test_get_all_members_from_db() {
        Assert.assertEquals( 7, service.getAllMembers().size() );
    }

    @Test
    public void test_add_member_to_db() {
        Member m = new Member( "cool", "boy", 2001, "cool@boy.be" );
        m.setChiroID( 1 );
        service.addMember(m);
        Assert.assertEquals( 8, service.getAllMembers().size() );
    }

    @Test(expected = Exception.class)
    public void test_add_empty_member_to_db(){
        service.addMember( null );
    }

    @Test
    public void test_delete_member_from_db() {
        service.deleteMember( 1 );
        Assert.assertEquals( 6, service.getAllMembers().size() );
    }

    @Test(expected = Exception.class)
    public void test_delete_empty_member_from_db() {
        service.deleteMember(0 );
    }


    @Test
    public void test_update_member() {
        Member m = service.getMember( 1 );
        Assert.assertEquals( 1995, service.getMember( 1 ).getBirthYear() );
        m.setBirthYear( 2005 );
        service.updateMember( m );
        Assert.assertEquals( 2005, service.getMember( 1 ).getBirthYear() );
    }

    @Test(expected = Exception.class)
    public void test_update_empty_member_from_db() {
        service.updateMember( null );
    }
}