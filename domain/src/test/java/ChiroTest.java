
import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jan on 15/02/2016.
 */

public class ChiroTest{

    Chiro c;
    Member m;

    @Before
    public void before(){
        c = new Chiro( "Gerolf", "g@g.be", "Genk" );
        m = new Member( "Jan", "P", 1995, "me@pekket.be" );
    }

    @Test
    public void test_create_chiro(){
        Assert.assertEquals( "Gerolf", c.getName() );
        Assert.assertEquals( "g@g.be", c.getEmail() );
        Assert.assertEquals( "Genk", c.getCity() );
    }

    @Test
    public void test_add_member_to_chiro(){
        c.addMember( m );
        Assert.assertEquals( 1, c.getMembers().size() );
    }


    @Test
    public void test_delete_member_from_chiro(){
        c.addMember( m );
        Assert.assertEquals( 1, c.getMembers().size() );
        c.deleteMember( m );
        Assert.assertEquals( 0, c.getMembers().size() );

    }


}
