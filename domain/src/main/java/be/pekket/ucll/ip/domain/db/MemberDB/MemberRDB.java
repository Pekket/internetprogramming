package be.pekket.ucll.ip.domain.db.MemberDB;

import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.model.Member;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class MemberRDB implements IMemberDB{

    private EntityManagerFactory factory;
    private EntityManager manager;
    
    public MemberRDB(String name) {
       factory = Persistence.createEntityManagerFactory(name);
       manager = factory.createEntityManager();
    }
    
    public void closeConnection() throws DbException {
        try {
            manager.close();
            factory.close(); 
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }
    
    @Override
    public Member getMember( long id ) throws DbException{
        try {
            Member member = manager.find(Member.class, id);
            return member;
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public List<Member> getAll() throws DbException{
         try {
            Query query = manager.createQuery("select s from Member s");
            return query.getResultList();
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public void addMember( Member member ) throws DbException{
          try {            
            manager.getTransaction().begin();
            manager.persist(member);
            manager.flush(); //Onmiddelijk op databank zetten
            manager.getTransaction().commit();                           
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public void deleteMember( long id ) throws DbException{
        try {
             manager.remove(id);
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public void updateMember( Member member ){
         try {
            deleteMember(member.getID());
            addMember(member);
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }
}
