package be.pekket.ucll.ip.domain.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties( ignoreUnknown = true )
public class Current{

    private Condition condition;

    public Condition getCondition(){
        return condition;
    }

    public void setCondition( Condition condition ){
        this.condition = condition;
    }

}
