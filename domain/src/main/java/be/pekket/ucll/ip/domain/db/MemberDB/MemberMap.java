package be.pekket.ucll.ip.domain.db.MemberDB;

import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.model.Member;

import java.util.ArrayList;
import java.util.List;

public class MemberMap implements IMemberDB{

    private List<Member> members;
    private int entityID;

    public MemberMap(){
        members = new ArrayList<Member>();
        entityID = 0;
    }

    @Override
    public Member getMember( long id ) throws DbException{
        if( id <= 0 )
            throw new DbException( "Incorrect ID : Cannot get member" );

        for(Member m : members) {
            if( m.getID() == id)
                return m;
        }
        return null;
    }

    @Override
    public List<Member> getAll() throws DbException{
        return members;
    }

    @Override
    public void addMember( Member member ) throws DbException{
        if(member == null)
            throw new DbException( "Cannot add empty member" );
        
        if(member.getID() == 0) member.setID(++entityID);
        members.add( member );        
    }

    @Override
    public void deleteMember( long id ) throws DbException{
        if(id <= 0)
            throw new DbException( "Incorrect ID : cannot delete member" );

        for(Member m : members) {
            if( m.getID() == id ) {
                members.remove( m );
                return;
            }
        }
    }

    @Override
    public void updateMember( Member member ){
        if(member == null)
            throw new DbException( "Cannot update empty member" );

        deleteMember( member.getID());
        addMember( member );
    }
}
