package be.pekket.ucll.ip.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Calendar;

@Entity
public class Member{

    @Id
    @GeneratedValue
    private long id;
    private long chiroID;

    @NotNull
    @Size( min = 1, max = 50, message = "{Size.Member.firstname}" )
    private String firstName;

    @NotNull
    @Size( min = 1, max = 50, message = "{Size.Member.lastname}" )
    private String lastName;

    @NotNull
    @Min( value = 1950, message = "{Min.Member.birthyear}" )
    @Max( value = 2016, message = "{Max.Member.birthyear}" )
    private int birthYear;

    @NotNull
    @Size( min = 1, max = 70, message = "{Size.Member.email}" )
    private String email;
    
    public Member(){
        id = 0;
    }

    public Member( String firstName, String lastName, int birthyear, String email ){
        setFirstName( firstName );
        setLastName( lastName );
        setBirthYear( birthyear );
        setEmail( email );
        id = 0;
    }

    // Functions

    public String calculateGroup(){
        int CurrentYear = Calendar.getInstance().get( Calendar.YEAR );
        int group = CurrentYear - birthYear;

        switch( group ){
            case 6:
            case 7:
            case 8:
                return "Speelclubs";
            case 9:
            case 10:
            case 11:
                return "Rakkers";
            case 12:
            case 13:
                return "Toppers";
            case 14:
            case 15:
                return "Kerels";
            case 16:
            case 17:
                return "Aspirant";
            default:
                return "Leider";
        }
    }

    // Getters and Setters
    
    public long getID() {
        return this.id;
    }
    
    public void setID(long id)  {
        if(id < 0 )
              throw new ModelException( "Invalid id" );
        this.id = id;
    }
    
    public String getFirstName(){
        return firstName;
    }

    public void setFirstName( String firstname ){
        this.firstName = firstname;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName( String lastName ){
        this.lastName = lastName;
    }

    public int getBirthYear(){
        return birthYear;
    }

    public void setBirthYear( int birthyear ){
        this.birthYear = birthyear;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail( String email ){
        this.email = email;
    }

    public long getChiroID(){
        return chiroID;
    }

    public void setChiroID( long id ){
        this.chiroID = id;
    }
}
