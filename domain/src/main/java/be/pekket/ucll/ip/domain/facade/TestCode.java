package be.pekket.ucll.ip.domain.facade;

import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;

/**
 * Created by Jan on 3/05/2016.
 */
public class TestCode{

    private Facade facade;

    public  TestCode( Facade facade ){

        this.facade = facade;

        //CHIRO GEROLF
        Chiro gerolf = new Chiro( "Sint-Gerolf Genk Centrum", "gerolf@abc.be", "Genk" );
        facade.addChiro( gerolf );

        Member jan = new Member( "Jan", "Pecquet", 1995, "jan@me.be" );
        jan.setChiroID( gerolf.getId() );

        Member karel = new Member( "Karel", "Karels", 2003, "karel@me.be" );
        karel.setChiroID( gerolf.getId() );
        Member tom = new Member( "Tom", "Trompet", 2005, "tom@me.be" );
        tom.setChiroID( gerolf.getId() );
        Member humpie = new Member( "Humpie", "Stumpie", 1999, "humpie@me.be" );
        humpie.setChiroID( gerolf.getId() );

        facade.addMember( jan );
        facade.addMember( karel );
        facade.addMember( tom );
        facade.addMember( humpie );

        // CHIRO LEUVEN
        Chiro leuven = new Chiro( "Chiro Leuven", "leuven@abc.be", "Leuven" );
        facade.addChiro( leuven );

        Member pieter = new Member( "Pieter", "Peeters", 1992, "pieter@me.be" );
        pieter.setChiroID( leuven.getId() );
        Member jaak = new Member( "Jaak", "Trekhaak", 2001, "jaak@me.be" );
        jaak.setChiroID( leuven.getId() );
        Member an = new Member( "An", "Pan", 2008, "an@me.be" );
        an.setChiroID( leuven.getId() );

        facade.addMember( pieter );
        facade.addMember( jaak );
        facade.addMember( an );
    }
}
