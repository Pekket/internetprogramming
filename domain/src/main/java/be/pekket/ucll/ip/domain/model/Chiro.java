package be.pekket.ucll.ip.domain.model;

import be.pekket.ucll.ip.domain.rest.WeatherRest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Chiro{

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Size( min = 1, max = 50, message = "{Size.Chiro.name}" )
    private String name;

    @NotNull
    @Size( min = 1, max = 70, message = "{Size.Chiro.email}" )
    private String email;

    @NotNull
    @Size( min = 1, max = 50, message = "{Size.Chiro.city}" )
    private String city;

    private List<Member> members;

    private String weather;
    
    public Chiro(){
        id = 0;
    }

    public Chiro( String name, String email, String city ){
        members = new ArrayList<Member>();
        setName( name );
        setEmail( email );
        setCity( city );
        id = 0;    }
    
    
    // Functions 

    public void addMember( Member member ){
        if( member == null )
            throw new ModelException( "Cannot add empty member to Chiro" );
        members.add( member );
    }

    public void deleteMember( Member member ){
        if( member == null )
            throw new ModelException( "Cannot delete empty member to Chiro" );
        members.remove( member );
    }

    
    // Getters and Setters
    
    public List<Member> getMembers(){
        return members;
    }

    public String getName(){
        return name;
    }

    public void setName( String name ){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail( String email ){
        this.email = email;
    }

    public String getCity(){
        return city;
    }

    public void setCity( String city ) {
        this.city = city;
    }
    
    public long getId() {
        return this.id;
    }
    
    public void setId(long id){
        this.id = id;
    }

    public String getWeather(){
        return weather;
    }

    public void setWeather( String weather ){
        this.weather = weather;
    }

    public void updateWeather(WeatherRest wr) {
        wr.getWeather( this );
    }

}
