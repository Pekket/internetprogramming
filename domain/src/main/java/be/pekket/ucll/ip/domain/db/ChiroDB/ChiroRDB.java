package be.pekket.ucll.ip.domain.db.ChiroDB;

import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class ChiroRDB implements IChiroDB{

    private EntityManagerFactory factory;
    private EntityManager manager;

    public ChiroRDB(String name){
        factory = Persistence.createEntityManagerFactory(name);
        manager = factory.createEntityManager();
    }
    
    public void closeConnection() throws DbException {
        try {
            manager.close();
            factory.close(); 
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public void addChiro( Chiro chiro ) throws DbException{
          try {
            manager.getTransaction().begin();
            manager.persist(chiro);
            manager.flush(); //Onmiddelijk op databank zetten
            manager.getTransaction().commit();                           
        } catch(Exception e) {
                throw new DbException(e.getMessage(), e);
        }           
    }

    @Override
    public Chiro getChiro( long id ) throws DbException{
       try {
            Chiro chiro = manager.getReference(Chiro.class, id);
            return chiro;
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public List<Chiro> getAll() throws DbException{
        try {
            Query query = manager.createQuery("select s from Chiro s");
            return query.getResultList();
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public void deleteChiro( long id ) throws DbException{
         try {
             manager.remove(getChiro(id));
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }

    @Override
    public void addMemberToChiro( Member member ) throws DbException{
        try {
             Chiro chiro = getChiro(member.getChiroID());
             chiro.addMember(member);
             updateChiro(chiro);
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        }  
    }

    @Override
    public void deleteMemberFromChiro( Member member ) throws DbException{
        try {
             Chiro chiro = getChiro(member.getChiroID());
             chiro.deleteMember(member);             
             updateChiro(chiro);
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        }  
    }

    @Override
    public void updateMemberFromChiro( Member member ) throws DbException{
        // TODO
    }

    @Override   
    public void updateChiro(Chiro chiro) throws DbException {
        try {
            deleteChiro(chiro.getId());
            addChiro(chiro);
        } catch(Exception e) {
            throw new DbException(e.getMessage(), e);
        } 
    }
}
