package be.pekket.ucll.ip.domain.db.ChiroDB;

import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChiroMap implements IChiroDB{

    private Map<Long, Chiro> chiros;
    private int entityID;

    public ChiroMap(){
        chiros = new HashMap<Long, Chiro>();
        entityID = 0;
    }

    @Override
    public Chiro getChiro( long id ) throws DbException{
        if( id <= 0 ||  !chiros.containsKey( id ) )
            throw new DbException( "Invalid id : Cannot get Chiro" );
        return this.chiros.get( id );
    }

    @Override
    public List<Chiro> getAll(){
        return new ArrayList(chiros.values());
    }

    @Override
    public void addChiro( Chiro chiro ) throws DbException{
        if( chiro == null )
            throw new DbException( "Cannot add empty Chiro" );
  
        if(chiro.getId() == 0)
            chiro.setId(++entityID );
        
        chiros.put( chiro.getId(), chiro );
    }
    
    @Override
    public void updateChiro(Chiro c) throws DbException {
        if( c == null )
            throw new DbException( "Cannot update empty Chiro" );
          Chiro chiro = getChiro(c.getId());
            
          chiro.setName(c.getName());
          chiro.setEmail(c.getEmail());
          chiro.setCity(c.getCity());        
    }

    @Override
    public void deleteChiro( long id ) throws DbException{
        if( id <= 0 || !chiros.containsKey( id ) )
            throw new DbException( "Invalid id : Cannot delete Chiro" );
        this.chiros.remove( id );
    }

    public void addMemberToChiro( Member member ){
        if( member == null || !chiros.containsKey( member.getChiroID())  )
            throw new DbException( "Cannot add empty Member or member already in db " );
        getChiro( member.getChiroID() ).addMember( member );
    }

    @Override
    public void deleteMemberFromChiro( Member member ) throws DbException{
        if( member == null || !chiros.containsKey( member.getChiroID()) )
            throw new DbException( "Cannot delete member from chiro" );

        chiros.get( member.getChiroID() ).deleteMember( member );
    }


    @Override
    public void updateMemberFromChiro(Member member) throws DbException {
        if( member == null || !chiros.containsKey( member.getChiroID()) )
        throw new DbException( "Cannot update member from chiro" );

        for(Member m : chiros.get(member.getChiroID()).getMembers()){
            if(m.getID() == member.getID()) {
                deleteMemberFromChiro( m );
                break;
            }
        }
        addMemberToChiro( member );
    }


}
