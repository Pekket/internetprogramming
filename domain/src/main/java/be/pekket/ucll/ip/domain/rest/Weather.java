package be.pekket.ucll.ip.domain.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties( ignoreUnknown = true )
public class Weather{

    private Current current;

    public Current getCurrent(){
        return current;
    }

    public void setCurrent( Current current ){
        this.current = current;
    }
}
