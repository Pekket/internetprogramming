package be.pekket.ucll.ip.domain.db.MemberDB;

import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.model.Member;

import java.util.List;

/**
 * Created by Jan on 15/02/2016.
 */
public interface IMemberDB{

    Member getMember(long id ) throws DbException;

    List<Member> getAll() throws DbException;

    void addMember( Member member ) throws DbException;

    void deleteMember(long id) throws DbException;

    void updateMember( Member member );

}
