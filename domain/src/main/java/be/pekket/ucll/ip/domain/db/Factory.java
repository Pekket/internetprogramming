package be.pekket.ucll.ip.domain.db;

import be.pekket.ucll.ip.domain.db.ChiroDB.ChiroMap;
import be.pekket.ucll.ip.domain.db.ChiroDB.ChiroRDB;
import be.pekket.ucll.ip.domain.db.ChiroDB.IChiroDB;
import be.pekket.ucll.ip.domain.db.MemberDB.IMemberDB;
import be.pekket.ucll.ip.domain.db.MemberDB.MemberMap;
import be.pekket.ucll.ip.domain.db.MemberDB.MemberRDB;

public class Factory{

    public static IChiroDB createChiroDB( String type ){
        IChiroDB db = null;
        if( type.equals( "MAP" ) ){
            db = new ChiroMap();
        }else if( type.equals( "DB" ) ){
            db = new ChiroRDB("ChiroPU");
        }
        return db;
    }


    public static IMemberDB createMemberDB( String type ){
        IMemberDB db = null;
        if( type.equals( "MAP" ) ){
            db = new MemberMap();
        }else if( type.equals( "DB" ) ){
            db = new MemberRDB("ChiroPU");
        }
        return db;
    }

}
