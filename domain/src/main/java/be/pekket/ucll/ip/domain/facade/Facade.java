package be.pekket.ucll.ip.domain.facade;

import be.pekket.ucll.ip.domain.db.ChiroDB.IChiroDB;
import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.db.Factory;
import be.pekket.ucll.ip.domain.db.MemberDB.IMemberDB;
import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;
import be.pekket.ucll.ip.domain.rest.WeatherRest;

import java.util.List;

public class Facade{

    private IChiroDB chiroDB;
    private IMemberDB memberDB;
    private WeatherRest weatherRest;

    public Facade( String type ) throws DbException{
        this.chiroDB = Factory.createChiroDB( type );
        this.memberDB = Factory.createMemberDB( type );
        weatherRest = new WeatherRest();
        TestCode t = new TestCode( this );
    }

    // CHIRO

    public Chiro getChiro( long id ) throws ServiceException{
        try{
            return this.chiroDB.getChiro( id );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public List<Chiro> getAllChiros() throws ServiceException{
        try{
            List<Chiro> chiros = this.chiroDB.getAll();
            updateWeatherChiros( chiros );
            return chiros;
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void addChiro( Chiro chiro ) throws ServiceException{
        try{
            this.chiroDB.addChiro( chiro );
        }catch( Exception e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void updateChiro( Chiro chiro ) throws ServiceException{
        try{
            this.chiroDB.updateChiro( chiro );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void deleteChiro( long id ) throws ServiceException{
        try{
            this.chiroDB.deleteChiro( id );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void deleteMemberFromChiro( Member member ) throws ServiceException{
        try{
            chiroDB.deleteMemberFromChiro( member );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void updateWeatherChiros( List<Chiro> chiros ) throws ServiceException{
        try{
            for( Chiro c : chiros )
                c.updateWeather(weatherRest);
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    // MEMBER

    public Member getMember( long id ) throws ServiceException{
        try{
            return memberDB.getMember( id );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public List<Member> getAllMembers() throws ServiceException{
        try{
            return memberDB.getAll();
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void addMember( Member member ) throws ServiceException{
        try{
            memberDB.addMember( member );
            chiroDB.addMemberToChiro( member );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void deleteMember( long id ) throws ServiceException{
        try{
            deleteMemberFromChiro( getMember( id ) );
            memberDB.deleteMember( id );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }

    public void updateMember( Member member ) throws ServiceException{
        try{
            System.out.println(member.getChiroID() + "JOEHOE");
            chiroDB.updateMemberFromChiro( member );
            memberDB.updateMember( member );
        }catch( DbException e ){
            throw new ServiceException( e.getMessage(), e );
        }
    }


}
