package be.pekket.ucll.ip.domain.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties( ignoreUnknown = true )
public class Condition{

    private String text;

    public String getText(){
        return text;
    }

    public void setText( String text ){
        this.text = text;
    }
}
