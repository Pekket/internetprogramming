/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pekket.ucll.ip.domain.db.ChiroDB;

import be.pekket.ucll.ip.domain.db.DbException;
import be.pekket.ucll.ip.domain.model.Chiro;
import be.pekket.ucll.ip.domain.model.Member;

import java.util.List;

public interface IChiroDB{

    Chiro getChiro( long id ) throws DbException;

    List<Chiro> getAll() throws DbException;

    void addChiro( Chiro chiro ) throws DbException;
    
    void updateChiro(Chiro chiro) throws DbException;

    void deleteChiro( long id ) throws DbException;

    void addMemberToChiro( Member member ) throws DbException;

    void deleteMemberFromChiro( Member member ) throws DbException;

    void updateMemberFromChiro(Member member) throws DbException;
}
