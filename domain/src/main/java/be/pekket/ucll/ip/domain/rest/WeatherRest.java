package be.pekket.ucll.ip.domain.rest;

import be.pekket.ucll.ip.domain.model.Chiro;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Jan on 3/05/2016.
 */
public class WeatherRest{

    public  void getWeather( Chiro chiro ){

        RestTemplate template = new RestTemplate();
        String location = chiro.getCity();
       try {
            Weather response = template.getForObject( "http://api.apixu.com/v1/current.json?key=fc10a80937e14c8396292013160305&q={location}"
                    , Weather.class, location );
            chiro.setWeather( response.getCurrent().getCondition().getText() );
        }catch( Exception e ){
            chiro.setWeather( "..." );
       }

    }
}
