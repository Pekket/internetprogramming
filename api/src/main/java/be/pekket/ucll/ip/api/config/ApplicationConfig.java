package be.pekket.ucll.ip.api.config;

import be.pekket.ucll.ip.domain.facade.Facade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan( "be.pekket.ucll.ip.api.controller" )
@EnableWebMvc
public class ApplicationConfig{

    @Bean
    public Facade service(){
        return new Facade( "MAP" );
    }
}
