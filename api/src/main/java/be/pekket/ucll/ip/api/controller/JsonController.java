package be.pekket.ucll.ip.api.controller;

import be.pekket.ucll.ip.domain.facade.Facade;
import be.pekket.ucll.ip.domain.model.Chiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping( "/" )
public class JsonController{

    @Autowired
    Facade service;

    @CrossOrigin(origins = "http://localhost:63342")
    @RequestMapping( method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    public List<Chiro> getAllChiros(){
        return service.getAllChiros();
    }

    @RequestMapping( value="/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    public Chiro getChiro( @PathVariable long id ){
        return service.getChiro( id );
    }

    @CrossOrigin(origins = "http://localhost:63342")
    @RequestMapping( method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity addChiro( @Valid @RequestBody Chiro chiro, BindingResult result ){
        if( result.hasErrors() )
            return new ResponseEntity<>( result, HttpStatus.BAD_REQUEST );

        service.addChiro( chiro );
        return new ResponseEntity<>( chiro, HttpStatus.OK );
    }


    @RequestMapping( method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity updateChiro( @Valid @RequestBody Chiro chiro, BindingResult result ){
        if( result.hasErrors() )
            return new ResponseEntity<>( result, HttpStatus.BAD_REQUEST );
        service.updateChiro( chiro );
        return new ResponseEntity<>( chiro, HttpStatus.OK );
    }

    @RequestMapping( value="/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE )
    public void deleteChiro( @PathVariable long id ){
        service.deleteChiro( id );
    }
}